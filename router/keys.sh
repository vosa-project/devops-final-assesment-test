#!/bin/bash

# Author - Katrin Loodus
#
# Date - 14.11.2016
# Version - 0.0.1

LC_ALL=C

# START

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/tmp/keys.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=192.168.6.2

    # Time to sleep between running the check again
    Sleep=5

    # Objective uname in VirtualTA
    Uname=keys

}

# User interaction: User creates public and private keys from desktop to server

KEYS () {

    while true 
    do 

	   # Check if user has created the needed keypair
        ssh root@$IP_to_SSH cat /home/student/.ssh/authorized_keys |grep student    

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nKeypair works! Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Keypair doesn't work! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

KEYS

exit 0

