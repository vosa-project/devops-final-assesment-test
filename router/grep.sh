#!/bin/bash

# Author - Katrin Loodus
#
# Date - 14.11.2016
# Version - 0.0.1

LC_ALL=C

# START

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/tmp/grep.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=192.168.6.2

    # Time to sleep between running the check again
    Sleep=5

    #  specific variables 

    # Objective uname in VirtualTA
    Uname=grep

}

# User interaction: Find out all the files that have ubuntu in them in /etc folder

GREP () {

    while true 
    do 

	   # Check if the user has found the correct files
        ssh root@$IP_to_SSH cat /home/student/ubuntu.log |grep os-release  

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\n Ubuntu word found! Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Ubuntu word not found! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

GREP

exit 0

